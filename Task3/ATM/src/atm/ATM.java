package atm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class ATM {

    private User currentUser;
    private final ArrayList<Transaction> transactionHistory;
    private final Scanner sc;
    private final int maxAttempts = 3;
    private int attemptsLogin = 0;
    private int attemptsWithdraw = 0;
    private int attemptsDeposit = 0;
    private int attemptsTrans = 0;

    public ATM() {
        this.transactionHistory = new ArrayList<>();
        this.sc = new Scanner(System.in);
    }

//     Method to handle user login
    public void login() {
        // Dummy user 
        User user = new User("123456", "7890", 2000.0);
        while (attemptsLogin < maxAttempts) {
            System.out.print("Enter user ID: ");
            String userId = sc.nextLine();

            System.out.print("Enter PIN: ");
            String pin = sc.nextLine();

            if (userId.equals(user.getUserId()) && pin.equals(user.getPin())) {
                currentUser = user;
                atmMenu();
                break;
            } else {
                attemptsLogin++;
                System.out.println("Invalid user ID or PIN.");
                if ((maxAttempts - attemptsLogin) == 0) {
                    System.out.println("Failed to login after 3 tries, call bank or try again later. ");
                    quit();
                }
                System.out.println("You have " + (maxAttempts - attemptsLogin) + " attempts left.");
            }
        }
    }

    // Method to display ATM functionalities
    private void atmMenu() {
        System.out.println("\n**TimeLess ATM Menu:**");
        System.out.println("1. View Transaction History");
        System.out.println("2. View Balance");
        System.out.println("3. Withdraw");
        System.out.println("4. Deposit");
        System.out.println("5. Transfer");
        System.out.println("6. Quit");
        System.out.print("Choose an option: ");
        int choice = sc.nextInt();
        sc.nextLine();

        switch (choice) {
            case 1:
                viewTransactionHistory();
                break;
            case 2:
                viewBalance();
                break;
            case 3:
                withdraw();
                break;
            case 4:
                deposit();
                break;
            case 5:
                transfer();
                break;
            case 6:
                quit();
                break;
            default:
                System.out.println("Invalid option. Please try again.");
                atmMenu();
        }
    }

    // Method to view transaction history
    private void viewTransactionHistory() {
        if (transactionHistory.isEmpty()) {
            System.out.print("No transaction made yet.");
            System.out.print("\n");
        } else {
            System.out.println("Transaction History:");
            transactionHistory.forEach(transaction -> {
                System.out.println(transaction.getType() + ": R" + transaction.getAmount()
                        + " " + transaction.getTimestamp());
            });
        }
        atmMenu();
    }

    // Method to handle balance
    private void viewBalance() {
        System.out.print("Balance: ");
        System.out.print(" R" + currentUser.getBalance());
        System.out.print("\n");
        atmMenu();
    }

    // Method to handle withdrawal
    private void withdraw() {
        while (attemptsWithdraw < maxAttempts) {
            System.out.print("Enter amount to withdraw: R");
            double amount = sc.nextDouble();
            sc.nextLine();

            if (amount <= 0) {
                attemptsWithdraw++;
                System.out.println("Invalid amount. Please enter a positive value.");
                System.out.println("You have " + (maxAttempts - attemptsWithdraw) + " attempts left.");
            } else if (amount > currentUser.getBalance()) {
                attemptsWithdraw++;
                System.out.println("Insufficient funds.");
                System.out.println("You have " + (maxAttempts - attemptsWithdraw) + " attempts left.");
            } else {
                double newBalance = currentUser.getBalance() - amount;
                currentUser.setBalance(newBalance);
                transactionHistory.add(new Transaction("Withdrawal", amount, getCurrentTimestamp()));
                System.out.println("R" + amount + " withdrawn successfully.");
                System.out.print("Current balance: R" + currentUser.getBalance());
                System.out.print("\n");
                break;
            }
        }
        if ((maxAttempts - attemptsWithdraw) == 0) {
            System.out.println("Failed to withdraw due to failed attempts ,check balance and try again later. ");
        }
        atmMenu();
    }

    // Method to handle deposit
    private void deposit() {
        while (attemptsDeposit < maxAttempts) {
            System.out.print("Enter amount to deposit: R");
            double amount = sc.nextDouble();
            sc.nextLine();

            if (amount <= 0) {
                attemptsDeposit++;
                System.out.println("Invalid amount. Please enter a positive value.");
                System.out.println("You have " + (maxAttempts - attemptsDeposit) + " attempts left.");
            } else {
                double newBalance = currentUser.getBalance() + amount;
                currentUser.setBalance(newBalance);
                transactionHistory.add(new Transaction("Deposit", amount, getCurrentTimestamp()));
                System.out.println("R" + amount + " deposited successfully.");
                System.out.print("Current balance: R" + currentUser.getBalance());
                System.out.print("\n");
                break;
            }

        }
        if ((maxAttempts - attemptsDeposit) == 0) {
            System.out.println("Failed to deposit due to failed attempts ,enter positive amounts and try again later. ");
        }
        atmMenu();
    }

    /// Method to handle transfer
    private void transfer() {
        while (attemptsTrans < maxAttempts) {

            System.out.print("Enter recipient's user ID: ");
            String recipientId = sc.nextLine();

            // Dummy recipient
            User recipient = new User("223345");

            if (!recipientId.equals(recipient.getUserId())) {
                attemptsTrans++;
                System.out.println("Recipient not found. Please enter a valid user ID.");
                System.out.println("You have " + (maxAttempts - attemptsTrans) + " attempts left.");
            } else {
                System.out.print("Enter amount to transfer: R");
                double amount = sc.nextDouble();
                sc.nextLine();
                if (amount <= 0) {
                    attemptsTrans++;
                    System.out.println("Invalid amount. Please enter a positive value.");
                    System.out.println("You have " + (maxAttempts - attemptsTrans) + " attempts left.");
                } else if (amount > currentUser.getBalance()) {
                    attemptsTrans++;
                    System.out.println("Insufficient funds.");
                    System.out.println("You have " + (maxAttempts - attemptsTrans) + " attempts left.");
                } else {
                    double newBalance = currentUser.getBalance() - amount;
                    currentUser.setBalance(newBalance);
                    transactionHistory.add(new Transaction("Transfer to " + recipientId, amount, getCurrentTimestamp()));
                    System.out.println("" + amount + " transferred successfully to user " + recipientId + ".");
                    System.out.print("Current balance: R" + currentUser.getBalance());
                    System.out.print("\n");
                    break;
                }
            }
        }
        if ((maxAttempts - attemptsTrans) == 0) {
            System.out.println("Failed to transfer due to failed attempts ,check balance and try again later. ");
        }
        atmMenu();
    }

    // Method to quit the ATM interface
    private void quit() {
        System.out.println("##Thank you for using TimeLess ATM. Hope to see you soon!##");
        System.exit(0);
    }

    // Method to get current timestamp
    private String getCurrentTimestamp() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return currentDateTime.format(formatter);
    }

    // Main method
    public static void main(String[] args) {
        ATM atm = new ATM();
        System.out.println("##Welcome to the TimeLess ATM!##");
        atm.login();

    }

}
