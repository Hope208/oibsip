package atm;

public class User {

    private final String userId;
    private final String pin;
    private double balance;
    
    public User(String userId) {
        this.userId = userId;
        this.pin = "";
        this.balance = 0.0;
    }

    public User(String userId, String pin, double balance) {
        this.userId = userId;
        this.pin = pin;
        this.balance = balance;
    }
    

    public String getUserId() {
        return userId;
    }

    public String getPin() {
        return pin;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
