package examtest;

public class User {

    private String userName;
    private String password;
    private String Name;
    private String email;
    private String number;

    public User(String userName, String password, String Name, String email, String number) {
        this.userName = userName;
        this.password = password;
        this.Name = Name;
        this.email = email;
        this.number = number;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
  
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
