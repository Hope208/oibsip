package examtest;

import java.util.Scanner;

public class ExamTest {

    private User currentUser;
    private final Scanner sc;
    private final int maxAttempts = 3;
    private int attemptsLogin = 0;

    public static void main(String[] args) {
        ExamTest et = new ExamTest();
        System.out.println("Please Login in before procceeding: ");
        et.login();
    }

    public ExamTest() {
        this.sc = new Scanner(System.in);
    }

    //     Method to handle user login
    public void login() {
        // Dummy user for demonstration
        User user = new User("hope208", "123456", "Hope Too", "hope@gmail.com", "0123456789");
        while (attemptsLogin < maxAttempts) {
            System.out.print("Enter user Name: ");
            String userName = sc.nextLine();

            System.out.print("Enter Password: ");
            String pass = sc.nextLine();

            if (userName.equals(user.getUserName()) && pass.equals(user.getPassword())) {
                currentUser = user;
                System.out.println("\n**Welcome back, " + currentUser.getUserName() + " **");
                examMenu();
                break;
            } else {
                attemptsLogin++;
                System.out.println("Invalid user ID or PIN.");
                if ((maxAttempts - attemptsLogin) == 0) {
                    System.out.println("Failed to login after 3 tries, contact admin or try again later. ");
                    logOut();
                }
                System.out.println("You have " + (maxAttempts - attemptsLogin) + " attempts left.");
            }
        }
    }

    //Exam menu
    private void examMenu() {
        System.out.println("\nWhat would you like to do today:");
        System.out.println("1. View Profile");
        System.out.println("2. Update Profile");
        System.out.println("3. Write Exam");
        System.out.println("4. Log Out");
        System.out.print("Choose an option: ");
        int choice = sc.nextInt();
        sc.nextLine();

        switch (choice) {
            case 1:
                viewProfile();
                break;
            case 2:
                updateProfile();
                break;
            case 3:
                String selectedModule = selectModule();
                writeExam(selectedModule);
                break;
            case 4:
                logOut();
                break;
            default:
                System.out.println("Invalid option. Please try again.");
                examMenu();
        }
    }

    // Method to viewProfile
    private void viewProfile() {
        System.out.print("User Profile: ");
        System.out.print("\nUser Name: " + currentUser.getUserName());
        System.out.print("\nName: " + currentUser.getName());
        System.out.print("\nCell Number: " + currentUser.getNumber());
        System.out.print("\nUser Email: " + currentUser.getEmail());
        System.out.print("\n");
        examMenu();
    }

    // Method to update Profile
    private void updateProfile() {
        System.out.println("\nWhich part would you like to update:");
        System.out.println("1. Password");
        System.out.println("2. Email address");
        System.out.println("3. Cell number");
        System.out.println("4. Back");
        System.out.print("Choose an option: ");
        int choice = sc.nextInt();
        sc.nextLine();

        switch (choice) {
            case 1:
                System.out.print("Enter new password: ");
                String newPassword = sc.nextLine();
                currentUser.setPassword(newPassword);
                System.out.println("Password updated successfully!");
                examMenu();
                break;
            case 2:
                System.out.print("Enter new email: ");
                String newEmail = sc.nextLine();
                currentUser.setEmail(newEmail);
                System.out.println("Email updated successfully!");
                examMenu();
                break;
            case 3:
                System.out.print("Enter new Cell NUmber");
                String newNumber = sc.nextLine();
                currentUser.setNumber(newNumber);
                System.out.println("Cell number updated successfully!");
                examMenu();
                break;
            case 4:
                examMenu();
                break;
            default:
                System.out.println("Invalid option. Please try again.");
                updateProfile();
        }
    }

    // Method to write Exam
    private String selectModule() {
        System.out.println("Select Module Exam:");
        System.out.println("1. Maths");
        System.out.println("2. Logic and Problem Solving");
        System.out.println("3. Literature");

        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();
        sc.nextLine();
        String moduleName = "";
        switch (choice) {
            case 1:
                moduleName = "Maths";
                System.out.println("You have selected Maths module.");
                break;
            case 2:
                moduleName = "Logic and Problem Solving";
                break;
            case 3:
                moduleName = "Literature";
                break;
            default:
                System.out.println("Invalid choice. Please select a valid module.");
                selectModule();
                break;
        }
        return moduleName;

    }

    // Function to write exam
    public void writeExam(String selectedModule) {
        String[][] questions;
        System.out.println("Taking Exam...");
        int totalQuestions = 10;
        int remainingQuestions = totalQuestions;
        int timeLimit = 30;
        long startTime = System.currentTimeMillis();

        switch (selectedModule) {
            case "Maths":
                questions = new String[][]{
                    {"What is the derivative of f(x) = 3x^2 +2x -5", "A) 6x + 2", "B) 6x - 2 ", "C) 6x + 1", "D) 6x"},
                    {"What is the integral of 3x with respect to x?", "A) x^2", "B) X^ + C", "C) x^2/2", "D) 2x^2"},
                    {"What is the limit of 1/x as x approaches infinity", "A) 0", "B) 1", "C) -1", "D) Does not exist"},
                    {"What is the solution to the equation 2^x = 16?", "A) 2", "B) 3", "C) 4", "D) 5"},
                    {"What is the square root of 144", "A) 12", "B) 22", "C) 14", "D) 16"},
                    {"What is the probability of rolling an even number on a fair six-sided die?", "A) 1/6", "B) 1/3", "C) 1/2", "D) 2/3"},
                    {"What is 20% of 250", "A) 40", "B) 70", "C) 50", "D) 60"},
                    {"What is the perimeter of a rectangle with length 8 units and width 6 units?", "A) 12 units", "B) 20 units", "C) 28 units", "D) 48 units"},
                    {"If x = 3 and y = 4, what is the value of x^2 + y^2?", "A) 5", "B) 7", "C) 9", "D) 25"},
                    {"What is the next number in the sequence: 2, 4, 8, 16, ...?", "A) 24", "B) 28", "C) 32", "D) 64"}
                };
                break;
            case "Literature":
                questions = new String[][]{
                    {"Who wrote 'To Kill a Mockingbird'?", "A) Mark Twain", "B) Harper Lee", "C) Charles Dickens", "D) J.K. Rowling"},
                    {"In which Shakespearean play does the character Hamlet appear?", "A) Macbeth", "B) Romeo and Juliet", "C) Hamlet", "D) Othello"},
                    {"Who is the author of 'Pride and Prejudice'?", "A) Jane Austen", "B) Emily Bronte", "C) Virginia Woolf", "D) Louisa May Alcott"},
                    {"In George Orwell's \"1984,\" what is the totalitarian regime called?", "A) Oceania", "B) Eurasia", "C) Eastasia", "D) Airstrip One"},
                    {"Who is the author of the poem \"The Waste Land\"?", "A) T.S. Eliot", "B) Robert Frost", "C) Hamlet", "D) Othello"},
                    {"In which novel do characters Ponyboy, Sodapop, and Dally appear?", "A) \"Brave New World\"", "B) \"The Outsiders\"", "C) \"Lord of the Flies\"", "D) \"The Great Gatsby\""},
                    {"What is the protagonist's name in Fyodor Dostoevsky's novel \"Crime and Punishment\"?", "A) Dmitri Karamazov", "B) Alyosha Karamazov", "C) Ivan Karamazov", "D) Raskolnikov"},
                    {"Which literary movement was characterized by its focus on individualism, emotion, and nature?", "A) Naturalism", "B) Modernism", "C) Realism", "D) Romanticism"},
                    {"In the poem \"The Road Not Taken,\" what is the central metaphor?", "A) A fork in the road", "B) A journey through life", "C) A forest", "D) A traveler"},
                    {"Which of the following is a play written by William Shakespeare?", "A) The Catcher in the Rye", "B) Pride and Prejudice", "C) Moby-Dick", "D) Romeo and Juliet"}
                };
                break;
            case "Logic and Problem Solving":
                questions = new String[][]{
                    {"What is the next number in the sequence: 1, 1, 2, 3, 5, 8, ...?", "A) 10", "B) 11", "C) 13", "D) 21"},
                    {"If all Bloops are Razzies and all Razzies are Lazzies, then are all Bloops Lazzies?", "A) Yes", "B) No", "C) Maybe", "D) I don't know"},
                    {"If A implies B and B implies C, then does A imply C?", "A) Yes", "B) No", "C) Sometimes", "D) It depends"},
                    {"If it takes 5 machines 5 minutes to make 5 widgets, how long would it take 100 machines to make 100 widgets?", "A) 5 minutes", "B) 25 minutes", "C) 100 minutes", "D) 500 minutes"},
                    {"How many squares are there on a standard chessboard?", "A) 36", "B) 49", "C) 64", "D) 81"},
                    {"If you have 3 switches in a room and each corresponds to a light bulb outside the room, how can you determine which switch corresponds to which light bulb if you can only enter the room once?",
                        "A) Wait until night and see which bulb is on", "B) Guess randomly", "C) Feel the temperature of the bulbs", "D) You can't determine"},
                    {"If a recipe calls for 2 cups of flour to make 12 cookies, how many cups of flour are needed to make 24 cookies?", "A) 2", "B) 3", "C) 6", "D) 4"},
                    {"A car travels at an average speed of 60 miles per hour for the first half of a trip and 40 miles per hour for the second half. What is the average speed for the entire trip?", "A) 45 mph", "B) 48 mph", "C) 50 mph", "D) 52 mph"},
                    {"In a group of 50 people, 30 have a cat, 20 have a dog, and 10 have both a cat and a dog. How many people in the group have neither a cat nor a dog?", "A) 0", "B) 10", "C) 20", "D) 30"},
                    {"A train travels at a speed of 60 miles per hour. How far will it travel in 2.5 hours?", "A) 120 miles", "B) 150 miles", "C) 160 miles", "D) 175 miles"}
                };
                break;
            default:
                System.out.println("Invalid module choice. Exiting.");
                return;
        }

        // Display questions and start timer
        System.out.println("You have selected " + selectedModule + " module.You have 30 mintues.");
        System.out.println("Here are your 10 exam questions:");

        for (int i = 0; i < questions.length; i++) {
            // Display question
            long currentTime = System.currentTimeMillis();
            long elapsedTimeSeconds = (currentTime - startTime) / 1000;
            long remainingTimeSeconds = timeLimit * 60 - elapsedTimeSeconds;
            long minutes = remainingTimeSeconds / 60;
            long seconds = remainingTimeSeconds % 60;
            System.out.printf("\n##Time Remaining: %02d:%02d##\n", minutes, seconds);

            System.out.println("\nQuestion " + (i + 1) + ": " + questions[i][0]);

            // Display options A-D
            for (int j = 1; j < questions[i].length; j++) {
                System.out.println(questions[i][j]);
            }

            System.out.print("Answer: ");
            String answer = sc.nextLine();
            remainingQuestions--;

            if (elapsedTimeSeconds >= timeLimit * 60) {
                System.out.println("Time is up! Auto-submitting the exam and logging out.Results will be sent to email");
                logOut();
                break;
            }
            if (remainingQuestions == 0) {
                System.out.println("All questions answered. Submitting the exam and logging out.Results will be sent to email");
                logOut();
            }
        }

    }

    // Method to quit the exam
    private void logOut() {
        System.out.println("##. Hope to see you soon, "
                + currentUser.getUserName() + "!");
        System.exit(0);
    }

}
