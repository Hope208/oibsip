package reservationSystem;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class CreateJsonFile {
    public static void main(String[] args) {
        // Create JSON objects for users, bookings, and trains
        JSONObject userObject = new JSONObject();
        userObject.put("username", "exampleUser");
        userObject.put("password", "examplePassword");

        JSONObject bookingObject = new JSONObject();
        bookingObject.put("username", "exampleUser");
        bookingObject.put("pnrNumber", "12345678");
        bookingObject.put("trainNumber", "T123");
        bookingObject.put("trainName", "Example Express");
        bookingObject.put("dateOfJourney", "2024-04-01");
        bookingObject.put("from", "City A");
        bookingObject.put("to", "City B");

        // Create an array to hold trains
        JSONArray trainsArray = new JSONArray();

        // Create multiple train objects
        JSONObject train1 = new JSONObject();
        train1.put("trainNumber", "T123");
        train1.put("trainName", "Example Express 1");
        train1.put("availableSeats", 50);
        train1.put("availableDates", "2024-04-01,2024-04-02");
        train1.put("from", "City A");
        train1.put("to", "City B");
		
		JSONObject train2 = new JSONObject();
        train2.put("trainNumber", "T456");
        train2.put("trainName", "Example Express 2");
        train2.put("availableSeats", 60);
        train2.put("availableDates", "2024-04-01,2024-04-02");
        train2.put("from", "City C");
        train2.put("to", "City D");

        // Add train objects to the array
        trainsArray.add(train1);
        trainsArray.add(train2);

        // Write the JSON array to a file
        writeJSONArrayToFile(trainsArray, "trains.json");

        System.out.println("JSON file created successfully.");

        // Create JSON arrays for multiple users, bookings, and trains
        JSONArray usersArray = new JSONArray();
        usersArray.add(userObject);

        JSONArray bookingsArray = new JSONArray();
        bookingsArray.add(bookingObject);

//        JSONArray trainsArray = new JSONArray();
//        trainsArray.add(trainObject);

        // Write JSON objects to files
        writeJSONArrayToFile(usersArray, "users.json");
        writeJSONArrayToFile(bookingsArray, "bookings.json");
        writeJSONArrayToFile(trainsArray, "trains.json");

        System.out.println("JSON files created successfully.");
    }

    private static void writeJSONArrayToFile(JSONArray jsonArray, String filename) {
        try (FileWriter file = new FileWriter(filename)) {
            file.write(jsonArray.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}