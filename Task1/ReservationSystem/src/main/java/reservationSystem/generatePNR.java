package reservationSystem;

import java.util.Random;

public class generatePNR {
    
    private String generatePNR() {
    // Define characters allowed in PNR number
    String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    StringBuilder pnr = new StringBuilder();
    Random random = new Random();

    // Generate a random PNR number of length 8
    for (int i = 0; i < 8; i++) {
        int randomIndex = random.nextInt(allowedChars.length());
        pnr.append(allowedChars.charAt(randomIndex));
    }

    return pnr.toString();
}
    
    
}
