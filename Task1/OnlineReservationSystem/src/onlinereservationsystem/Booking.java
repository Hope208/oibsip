package onlinereservationsystem;
import java.util.Date;

public class Booking {
    private String username;
    private String pnrNumber;
    private String trainNumber;
    private String trainName;
    private Date dateOfJourney;
    private String from;
    private String to;
    private double price;

    public Booking(String username, String pnrNumber, String trainNumber, String trainName, Date dateOfJourney, String from, String to, double price) {
        this.username = username;
        this.pnrNumber = pnrNumber;
        this.trainNumber = trainNumber;
        this.trainName = trainName;
        this.dateOfJourney = dateOfJourney;
        this.from = from;
        this.to = to;
        this.price = price;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPnrNumber() {
        return pnrNumber;
    }

    public void setPnrNumber(String pnrNumber) {
        this.pnrNumber = pnrNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public Date getDateOfJourney() {
        return dateOfJourney;
    }

    public void setDateOfJourney(Date dateOfJourney) {
        this.dateOfJourney = dateOfJourney;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
}
