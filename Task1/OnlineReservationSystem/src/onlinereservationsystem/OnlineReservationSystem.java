package onlinereservationsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class OnlineReservationSystem {

    private User currentUser;
//    private Booking booking;
//    private Train trains;
    private static final List<Train> trainList = new ArrayList<>();
    private static final List<Booking> bookingList = new ArrayList<>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final Random random = new Random();
    private final Scanner sc;
    private final int maxAttempts = 3;
    private int attemptsLogin = 0;

    public static void main(String[] args) {
        OnlineReservationSystem ors = new OnlineReservationSystem();
        populateDummyData();
        System.out.println("Please Login in before procceeding: ");
        ors.login();
    }

    public OnlineReservationSystem() {
        this.sc = new Scanner(System.in);

    }

    private void login() {
        // Dummy user for demonstration
        User user = new User("hope208", "123456", "Hope Too", "hope@gmail.com", "0123456789");
        while (attemptsLogin < maxAttempts) {
            System.out.print("Enter user Name: ");
            String userName = sc.nextLine();

            System.out.print("Enter Password: ");
            String pass = sc.nextLine();

            if (userName.equals(user.getUserName()) && pass.equals(user.getPassword())) {
                currentUser = user;
                System.out.println("\n**Welcome back, " + currentUser.getUserName() + " **");
                onlineMenu();
                break;
            } else {
                attemptsLogin++;
                System.out.println("Invalid user ID or PIN.");
                if ((maxAttempts - attemptsLogin) == 0) {
                    System.out.println("Failed to login after 3 tries, contact admin or try again later. ");
                    logOut();
                }
                System.out.println("You have " + (maxAttempts - attemptsLogin) + " attempts left.");
            }
        }
    }

    private void onlineMenu() {
        System.out.println("\nWhat would you like to do today:");
        System.out.println("1. View Profile");
        System.out.println("2. View Reservation");
        System.out.println("3. Make Rervation");
        System.out.println("4. Cancel Reservation");
        System.out.println("5. Log out");
        System.out.print("Choose an option: ");
        int choice = sc.nextInt();
        sc.nextLine();

        switch (choice) {
            case 1:
                viewProfile();
                break;
            case 2:
                viewReservation();
                break;
            case 3:
                makeReservation();
                break;
            case 4:
                cancelReservation();
                break;
            case 5:
                logOut();
                break;
            default:
                System.out.println("Invalid option. Please try again.");
                onlineMenu();
        }
    }

    private void viewProfile() {
        System.out.print("User Profile: ");
        System.out.print("\nUser Name: " + currentUser.getUserName());
        System.out.print("\nName: " + currentUser.getName());
        System.out.print("\nCell Number: " + currentUser.getNumber());
        System.out.print("\nUser Email: " + currentUser.getUserName());
        System.out.print("\n");
        onlineMenu();
    }

    private void viewReservation() {
        boolean foundBooking = false;

        System.out.println("Your bookings:");

        for (Booking booking : bookingList) {
            if (booking.getUsername().equals(currentUser.getUserName())) {
                foundBooking = true;
                // Display booking details
                System.out.println("PNR Number: " + booking.getPnrNumber());
                System.out.println("Train Number: " + booking.getTrainNumber());
                System.out.println("Train Name: " + booking.getTrainName());
                System.out.println("Date of Journey: " + booking.getDateOfJourney());
                System.out.println("From: " + booking.getFrom());
                System.out.println("To: " + booking.getTo());
                System.out.println("Price: " + booking.getPrice());
                System.out.println("---------------------------------");
            }
        }

        if (!foundBooking) {
            System.out.println("No bookings made.");
        }
        onlineMenu();
    }

    private void makeReservation() {
        // Parse date
        Date dateOfJourney;
        System.out.println("Enter date of journey (yyyy-MM-dd):");
        String dateString = sc.nextLine();

        try {
            dateOfJourney = dateFormat.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Invalid date format. Please enter date in yyyy-MM-dd format.");
            return;
        }

        // Display available locations for from and to
        System.out.println("Available locations:");
        System.out.println("1. Johannesburg");
        System.out.println("2. Sandton");
        System.out.println("3. Centurion");
        System.out.println("4. Germiston");
        System.out.println("5. Pretoria");

        // From
        System.out.print("Enter the number of the origin location: ");
        int originIndex = sc.nextInt();
        sc.nextLine();
        if (originIndex < 1 || originIndex > 5) {
            System.out.println("Invalid option for origin location.");
            makeReservation();
        }

        // To
        System.out.print("Enter the number of the destination location: ");
        int destIndex = sc.nextInt();
        sc.nextLine();
        if (destIndex < 1 || destIndex > 5 || destIndex == originIndex) {
            System.out.println("Invalid option for destination location.");
            makeReservation();
        }

        String[] locations = {"Johannesburg", "Sandton", "Centurion", "Germiston", "Pretoria"};
        String from = locations[originIndex - 1];
        String to = locations[destIndex - 1];

        List<Train> availableTrains = findAvailableTrains(dateOfJourney, from, to);
        if (availableTrains.isEmpty()) {
            System.out.println("No trains available for the selected date and route.");
            System.out.println("Do you want to 1.Go back to menu or 2.make reservation with different choices.");
            String doNext = sc.nextLine();
            if ("1".equals(doNext)) {
                onlineMenu();
            } else {
                makeReservation();
            }
            return;
        }

        System.out.println("Available trains:");
        for (int i = 0; i < availableTrains.size(); i++) {
            System.out.println((i + 1) + ". " + availableTrains.get(i).getTrainName());
        }

        System.out.print("Enter the number of the train to book: ");
        int trainIndex = sc.nextInt();
        sc.nextLine();
        if (trainIndex < 1 || trainIndex > availableTrains.size()) {
            System.out.println("Invalid train number.");
            return;
        }

        Train selectedTrain = availableTrains.get(trainIndex - 1);

        // Generate random PNR number
        String pnrNumber = generatePNR();

        // Create booking
        Booking booking = new Booking(currentUser.getUserName(), pnrNumber, selectedTrain.getTrainNumber(),
                selectedTrain.getTrainName(), dateOfJourney, from, to, selectedTrain.getPrice());

        // Update available seats for the selected train
        selectedTrain.updateAvailableSeats(1);

        bookingList.add(booking);

        System.out.println("Booking successful. Your PNR number is: " + pnrNumber);
        System.out.println("Keep your PNR number safe as you would need it in case of cancellation");
    }

    private void cancelReservation() {
        System.out.println("Enter PNR number to cancel the reservation: ");
        String pnrNumber = sc.nextLine();

        // Search for the booking with the given PNR number
        boolean found = false;
        for (Booking booking : bookingList) {
            if (booking.getPnrNumber().equals(pnrNumber)) {
                found = true;
                System.out.println("Booking found:");
                System.out.println("PNR Number: " + booking.getPnrNumber());
                System.out.println("Train Number: " + booking.getTrainNumber());
                System.out.println("Train Name: " + booking.getTrainName());
                System.out.println("Date of Journey: " + booking.getDateOfJourney());
                System.out.println("From: " + booking.getFrom());
                System.out.println("To: " + booking.getTo());
                System.out.println("Price: " + booking.getPrice());

                // Confirm cancellation
                System.out.print("Do you want to cancel this booking? (yes/no): ");
                String confirmation = sc.nextLine();
                if (confirmation.equalsIgnoreCase("yes")) {
                    bookingList.remove(booking);
                    System.out.println("Booking cancelled successfully.");
                    onlineMenu();
                } else {
                    System.out.println("Cancellation cancelled.");
                    onlineMenu();
                }
                break;
            }
        }
        if (!found) {
            System.out.println("Booking not found with PNR number: " + pnrNumber);
        }
    }

    private static List<Train> findAvailableTrains(Date dateOfJourney, String from, String to) {
        List<Train> availableTrains = new ArrayList<>();
        trainList.stream().filter(train -> (train.getAvailableDates().contains(dateOfJourney)
                && train.getPlacesFromTo().contains(from) && train.getPlacesFromTo().contains(to) && train.isAvailable())).forEachOrdered(train -> {
            availableTrains.add(train);
        });
        return availableTrains;
    }

    private static String generatePNR() {
        // Generate a random 6-digit number
        return String.format("%06d", random.nextInt(1000000));
    }

    private static void populateDummyData() {
        // Example 1
        List<Date> availableDates1 = Arrays.asList(new Date(2023, 3, 29), new Date(2023, 3, 30));
        List<String> placesFromTo1 = Arrays.asList("Johannesburg", "Sandton");
        Train train1 = new Train("001", "Express1", "Johannesburg Park Station", 100,
                availableDates1, placesFromTo1, true, 50.0);
        trainList.add(train1);

        // Example 2
        List<Date> availableDates2 = Arrays.asList(new Date(2023, 3, 30), new Date(2023, 3, 31));
        List<String> placesFromTo2 = Arrays.asList("Sandton Station", "Centurion");
        Train train2 = new Train("002", "Express2", "Sandton Station", 120,
                availableDates2, placesFromTo2, true, 60.0);
        trainList.add(train2);

        // Example 3
        List<Date> availableDates3 = Arrays.asList(new Date(2023, 3, 31), new Date(2023, 4, 1));
        List<String> placesFromTo3 = Arrays.asList("Centurion", "Germiston");
        Train train3 = new Train("003", "Express3", "Centurion Station", 80,
                availableDates3, placesFromTo3, true, 45.0);
        trainList.add(train3);

        // Example 4
        List<Date> availableDates4 = Arrays.asList(new Date(2023, 4, 1), new Date(2023, 4, 2));
        List<String> placesFromTo4 = Arrays.asList("Germiston", "Pretoria");
        Train train4 = new Train("004", "Express4", "Germiston Station", 150,
                availableDates4, placesFromTo4, true, 70.0);
        trainList.add(train4);

        // Example 5
        List<Date> availableDates5 = Arrays.asList(new Date(2023, 4, 2), new Date(2023, 4, 3));
        List<String> placesFromTo5 = Arrays.asList("Pretoria", "Johannesburg");
        Train train5 = new Train("005", "Express5", "Pretoria Station", 100,
                availableDates5, placesFromTo5, true, 55.0);
        trainList.add(train5);

        // Example 6
        List<Date> availableDates6 = Arrays.asList(new Date(2023, 4, 3), new Date(2023, 4, 4));
        List<String> placesFromTo6 = Arrays.asList("Johannesburg", "Centurion");
        Train train6 = new Train("006", "Express6", "Johannesburg Park Station", 120,
                availableDates6, placesFromTo6, true, 60.0);
        trainList.add(train6);

        // Example 7
        List<Date> availableDates7 = Arrays.asList(new Date(2023, 3, 29), new Date(2023, 3, 30));
        List<String> placesFromTo7 = Arrays.asList("Johannesburg", "Sandton");
        Train train7 = new Train("001", "Express1", "Johannesburg Park Station", 100,
                availableDates7, placesFromTo7, true, 45.0);
        trainList.add(train7);
    }

    private void logOut() {
        System.out.println("##. Hope to see you soon, "
                + currentUser.getUserName() + "!");
        System.exit(0);
    }

}
