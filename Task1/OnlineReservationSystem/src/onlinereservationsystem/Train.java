package onlinereservationsystem;
//import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Train {
    private String trainNumber;
    private String trainName;
    private String trainStation;
    private int numberOfSeats;
    private List<Date> availableDates;
    private List<String> placesFromTo;
    private boolean available;
    private double price;

    public Train(String trainNumber, String trainName, String trainStation, int numberOfSeats, List<Date> availableDates, List<String> placesFromTo, boolean available, double price) {
        this.trainNumber = trainNumber;
        this.trainName = trainName;
        this.trainStation = trainStation;
        this.numberOfSeats = numberOfSeats;
        this.availableDates = availableDates;
        this.placesFromTo = placesFromTo;
        this.available = available;
        this.price = price;
    }
    

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getTrainStation() {
        return trainStation;
    }

    public void setTrainStation(String trainStation) {
        this.trainStation = trainStation;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public List<Date> getAvailableDates() {
        return availableDates;
    }

    public void setAvailableDates(List<Date> availableDates) {
        this.availableDates = availableDates;
    }

    public List<String> getPlacesFromTo() {
        return placesFromTo;
    }

    public void setPlacesFromTo(List<String> placesFromTo) {
        this.placesFromTo = placesFromTo;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }   
    
    public void updateAvailableSeats(int seatsBooked) {
        numberOfSeats -= seatsBooked;
    }
    
}
