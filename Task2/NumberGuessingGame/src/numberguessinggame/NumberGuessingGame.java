package numberguessinggame;

import java.util.Random;
import java.util.Scanner;

public class NumberGuessingGame {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            Random random = new Random();

            int min = 1;
            int max = 100;
            int maxAttempts = 5;
            int totalRounds = 3;
            int totalScore = 0;

            System.out.println("Lets test your guessing skill!");
            System.out.println("You have a total of 5 attemps for every 3 rounds.Good Luck!");

            for (int round = 1; round <= totalRounds; round++) {
                int randomNum = random.nextInt(max - min + 1) + min;
                int attempts = 0;
                int roundScore;

                System.out.println("\nRound " + round + ": ");
                System.out.println("I have chosen a number between " + min + " and " + max + ".");
//                System.out.println(randomNum);

                while (attempts < maxAttempts) {
                    System.out.print("Attemps " + (attempts + 1) + "/" + maxAttempts + " : Enter your guess: ");
                    int guess = sc.nextInt();
                    attempts++;

                    if (min < guess && guess < max) {
                        if (guess == randomNum) {
                            roundScore = maxAttempts - attempts + 1;
                            totalScore += roundScore;
                            System.out.println("Congratulations! You guessed the number in " + attempts + " attempts.");
                            System.out.println("Round " + round + " Score: " + roundScore);
                            break;
                        } else if (guess < randomNum) {
                            System.out.println("Too low! Try again.");
                        } else {
                            System.out.println("Too high! Try again.");
                        }
                    } else {
                        System.out.println("Only guess numbers between 1 and 100");
                    }
                }

                if (attempts == maxAttempts) {
                    System.out.println("So close but you are out of attempts for round " + round + " . The number was: " + randomNum);
                }

                System.out.println("Current Total Score : " + totalScore);
            }

            System.out.println("\nGame Over!");
            System.out.println("Your Final Score : " + totalScore + " out of 15");
            if (totalScore > 12) {
                System.out.println("You are a Guessing Pro!!!");
            } else {
                System.out.println("Better Luck next time, For now keep Guessing.");
            }
        }
    }
}
